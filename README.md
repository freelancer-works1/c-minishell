# C-Minishell

This is a tiny shell that emulates bash behaviour.

It's written in `C` and implements the following utitlities:

 - Pipes
 - Fork
 - Umask
 - Chdir
 - Strtol

It supports command sequences and redirection of input and output.